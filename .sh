
alias ls='ls -GpF'

curdir=$(pwd)
olsdir="/ols-banner-template"
currolsdir=$curdir$olsdir
alias temp_down="git clone https://Dheeajreddy@bitbucket.org/Dheeajreddy/ols-banner-template.git"
alias gotoOls="cd ols-banner-template"
alias link_gulp="sudo npm link gulp gulp-concat gulp-uglify gulp-replace-path gulp-inline-source gulp-minify-css gulp-autoprefixer gulp-rename gulp-sass gulp-useref gulp-notify gulp-plumber gulp-zip gulp-bump gulp-confirm gulp-imagemin gulp-insert-lines gulp-dom del fs path imagemin-pngquant run-sequence browser-sync"

function code() {
    if [[ $# = 0 ]]
    then
        open -a "Visual Studio Code"
    else
        [[ $1 = /* ]] && F="$1" || F="$PWD/${1#./}"
        open -a "Visual Studio Code" --args "$F"
    fi
}
function host() { http-server -p  "$1" ; }
function renameOls() { mv $currolsdir "${curdir}/${1}"; }
function gotoDir() { cd "$1"}
function masterOls { (pwd && temp_down && renameOls "$1" && code "$1" && cd "$1" &&  link_gulp && gulp) ;}
function subOls { (pwd && temp_down && renameOls "$1"  && cd "$1" && stt && link_gulp && gulp) ;}
function reTest(){ browser-sync reload --port "$1" --reload-delay 1000 ;}
function testOls(){ (cd _publishZip && cd */ && browser-sync start -s -f "*.html" -b "firefox" -b "google chrome" -b "safari") ;}
function makezip() { zip -r "${1%%/}.zip" "$1" ; }
function syncTest(){browser-sync start -s -f "*.html" -b "google chrome"}