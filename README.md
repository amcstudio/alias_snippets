Procedure:-

1.please paste the code in "zshrc.txt" file at the end of your shell file.
it either ".zshrc" or ".bashrc" which is located in below location
path:-- "/Users/mac name /.zshrc";

2.Do not replace just add at the end of the .zshrc/.bashrc

3. type ~/.zshrc or ~/.bashrc

Useful command:-

Following are the commands for the usage
1."masterOls filename" :-
following is the procedure to use the this command

A.open terminal at the folder where you want to create master file.
B.Type command with the name of the file.
example:- if we want to name the file "FCB_001_300x250" use command
masterOls FCB_001_300x250

    it will open the file in vscode if you have installed it with running gulp

2. "subOls filename" :--
   if you have sublime not vscode and follow above steps

3."temp_down" :- it is command used to clone banner template from the bitbucket

4. "link_gulp" :- it is command used for the link the plugins of the gulp for the folder.

5."host portname" :-- open terminal at the folder in which you want to start http-server
type host command with preffered port number like below
host 3002
it will open the http-server at that port

6."testOls" :- open terminal at the folder which is having \_publishZip folder with unzipped files
type testOls
It will open file in three browsers i.e. chrome, firefox and run simultaneously.

7. "syncTest" :- open terminal at the folder which you will wish to open in browser
   type syncTest
   It will open file in two browsers i.e. chrome, firefox and run simultaneously.
